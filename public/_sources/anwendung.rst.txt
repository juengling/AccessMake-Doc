Wie verwende ich AccessMake?
============================

Hilfe
-----

AccessMake ist ein reines Kommandozeilenprogramm. Wie bei vielen
Programmen kann man einen ersten Eindruck von den Fähigkeiten bekommen,
indem man auf der Kommandozeile folgenden Befehl eingibt:

::

   amake --help

Dies ergibt folgende Ausgabe:

::

   Access Make v0.7.0
   usage: Access Make [-h] [-v] [config]

   Help the MS Access developer (and others) to create a build environment for
   continuous integration.

   positional arguments:
     config         Name/path of the configuration file. Default is "amakefile"
                    in the current folder.

   optional arguments:
     -h, --help     show this help message and exit
     -v, --verbose  Increase verbosity level

Nun wissen wir, dass als erstes Argument ein Konfigurationsfile
angegeben werden kann. Wird dies nicht angegeben, geht das Programm
davon aus, dass es ein ``amakefile`` im aktuellen Arbeitsverzeichnis
gibt.

Alle Pfadangaben beziehen sich jeweils relativ auf den Standort dieser
Datei, egal ob der Pfad dorthin nun angegeben wurde oder nicht. Dadurch
müssen in der Konfigurationsdatei keine absoluten Pfade verwendet werden
und der Befehl sollte in allen Verzeichnissen funktionieren, egal wohin
das Projekt geclont wird.

Das AMakefile
-------------

Eine Zeile im AMakefile kann sein:

-  Leerzeile
-  Eine Kommentarzeile mit einem # am Anfang
-  Eine Zeile mit einem Befehl

Momentan werden folgende Befehle unterstützt:

-  read
-  write
-  get

Der READ-Befehl
~~~~~~~~~~~~~~~

Der READ-Befehl liest aus einer angegebenen Datei einen Wert. Die
Befehlszeile ist folgendermaßen aufgebaut:

::

   READ dateiname USING regex 

Der WRITE-Befehl
~~~~~~~~~~~~~~~~

Der GET-Befehl
~~~~~~~~~~~~~~
.. AccessMake documentation master file, created by
   sphinx-quickstart on Sat Jul  7 11:09:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AccessMake's documentation!
======================================

WORK IN PROGRESS

Diese Dokumentation ist noch im Aufbau!

.. toctree::
   :maxdepth: 2
   :caption: Inhalt:
   
   historie
   hintergrund
   installation
   anwendung


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
